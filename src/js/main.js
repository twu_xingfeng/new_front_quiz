import Person from './person';
import $ from 'jquery';

const getPersonInfo = url => {
  return new Promise((resolve, reject) => {
    fetch(url).then(
      res => {
        if (res.ok) {
          res.json().then(data => {
            resolve(data);
          });
        } else {
          reject(res.status);
        }
      },
      e => {
        reject(e);
      }
    );
  });
};

const renderToHtml = data => {
  const person = new Person(
    data.name,
    data.age,
    data.description,
    data.educations
  );
  document.getElementById('list_container').innerHtml = '';
  document.getElementById('myinfo').innerText = person.myinfo();
  document.getElementById('about').innerText = person.description;
  $.each(person.educations, (k, v) => {
    let html =
      '<li><section><div class="edu_left"><h1>' +
      v.year +
      '</h1></div><div class="edu_right">' +
      '<h1>' +
      v.title +
      '</h1><p>' +
      v.description +
      '</p>' +
      '</div></section></li>';
    $('#list_container').append(html);
  });
};

const renderPerson = () => {
  getPersonInfo('http://localhost:3000/person')
    .then(data => {
      renderToHtml(data);
    })
    .catch(e => {
      alert(e);
    });
};

export { renderPerson };
